import 'package:flutter/material.dart';

class ListSnack extends StatefulWidget {
  ListSnack({Key? key}) : super(key: key);

  @override
  _ListSnack createState() => _ListSnack();
}

class _ListSnack extends State<ListSnack> {
  var questionValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var question = [
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
    'ขนม 300 Kcal',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffFFA041),
        title: Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            "ผลไม้ / ขนม",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValue[0],
              title: Text(question[0]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[1],
              title: Text(question[1]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[2],
              title: Text(question[2]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[3],
              title: Text(question[3]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[4],
              title: Text(question[4]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[5],
              title: Text(question[5]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[6],
              title: Text(question[6]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[7],
              title: Text(question[7]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[8],
              title: Text(question[8]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[9],
              title: Text(question[9]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[10],
              title: Text(question[10]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[10] = newValue!;
                });
              }),
          Container(
            height: 80,
            child: ElevatedButton(
              onPressed: () async {
                Navigator.pop(context);
              },
              child: const Text(
                'บันทึกรายการอาหาร',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'Calories_1.dart';

class CalculatorBMI extends StatelessWidget {
  const CalculatorBMI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff386672),
          title: Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              "Table Calories",
              style: TextStyle(
                color: Colors.white,
                fontSize: 28,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Messagetext(),
        ));
  }
}

class Messagetext extends StatefulWidget {
  Messagetext({Key? key}) : super(key: key);

  @override
  _MessagetextState createState() => _MessagetextState();
}

class _MessagetextState extends State<Messagetext> {
  String w = '0';
  String t = '0';
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 50,
        ),
        Image.asset(
          'images/Bare.png',
          width: 200,
          height: 200,
        ),
        SizedBox(
          height: 15,
        ),
        Text(
          "BMI หรือ ดัชนีมวลกาย",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Container(
          child: Text('ใช้ชี้วัดความสมดุลของนํ้าหนักตัวและส่วนสูง'),
          margin: EdgeInsets.only(top: 5),
        ),
        Container(
            child: Text('ซึ่งช่วยระบุได้ว่าตอนนี้รูปร่างผอมหรือว่าอ้วนเกินไป'),
            margin: EdgeInsets.only(top: 5)),
        SizedBox(
          height: 15,
        ),
        Container(
          padding: EdgeInsets.only(left: 410, right: 410),
          child: TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: 'นํ้าหนักตัว (kg.)'),
            onChanged: (value) {
              setState(() {
                w = value;
              });
              print(w);
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 410, right: 410),
          child: TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: 'ส่วนสูง (cm.)'),
            onChanged: (value) {
              setState(() {
                t = value;
              });
              print(t);
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 15),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: Color(0xff808080)),
              child: Text('ผอมไป 0 - 18.5',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, left: 10),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: Color(0xff8FCE99)),
              child: Text('สมส่วน 18.5 - 23',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, left: 10),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: Color(0xffF3F316)),
              child: Text('นน.เกิน 23 - 30',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, left: 10),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: Color(0xffFFA734)),
              child: Text(
                'อ้วน 30 - 40',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 15, left: 10),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: Color(0xffF93939)),
              child: Text(
                'อ้วนมาก > 40',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.all(20),
              width: 300,
              height: 100,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Color(0xffFF4E4E), padding: EdgeInsets.all(20)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CaloriespageOne()),
                    );
                  },
                  child: Text(
                    'ยกเลิก',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  )),
            ),
            Container(
              padding: EdgeInsets.all(20),
              width: 300,
              height: 100,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Color(0xff287891), padding: EdgeInsets.all(20)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CaloriespageOne()),
                    );
                  },
                  child: Text(
                    'บันทึก',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  )),
            )
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'Calories_1.dart';

class Profilefrom extends StatelessWidget {
  const Profilefrom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff5f6fa),
      body: ListView(
        children: [InputFrom()],
      ),
    );
  }
}

class InputFrom extends StatefulWidget {
  InputFrom({Key? key}) : super(key: key);

  @override
  _InputFromState createState() => _InputFromState();
}

class _InputFromState extends State<InputFrom> {
  String male = 'ชาย';
  String female = 'หญิง';
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Text(
            'เพื่อการคำนวณแคลอรี่ที่เหมาะสมสำหรับคุณ',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'กรุณาบอกเราเกี่ยวกับตัวคุณ',
            style: TextStyle(
                color: Colors.black38,
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
          Container(
            child: Image.asset(
              'images/gender.jpg',
              width: 250,
              height: 250,
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 410, right: 410),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: 'อายุ (ปี.)'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 410, right: 410),
            child: TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: 'เพศ'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 410, right: 410),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: 'ส่วนสูง (ซม.)'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 410, right: 410),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: 'นํ้าหนัก (กก.)'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 410, right: 410),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration:
                  InputDecoration(labelText: 'นํ้าหนักที่ต้องการ (กก.)'),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            width: 300,
            height: 100,
            decoration:
                BoxDecoration(borderRadius: BorderRadius.circular(25.0)),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color(0xff287891), padding: EdgeInsets.all(20)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CaloriespageOne()),
                  );
                },
                child: Text(
                  'ถัดไป',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                )),
          )
        ],
      ),
    );
  }
}

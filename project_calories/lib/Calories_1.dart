import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_cards/flutter_custom_cards.dart';

import 'Calories_2.dart';
import 'Calories_3.dart';
import 'Calories_4.dart';
import 'Calories_5.dart';

class CaloriespageOne extends StatelessWidget {
  const CaloriespageOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Row(
            children: [
              Text(
                'Logout',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              IconButton(
                  onPressed: () async {
                    await FirebaseAuth.instance.signOut();
                  },
                  icon: Icon(Icons.logout))
            ],
          )
        ],
        backgroundColor: Color(0xff386672),
        title: Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            "Table Calories",
            style: TextStyle(
              color: Colors.white,
              fontSize: 28,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          CardProfile(),
          CardBMI(
            heightII: 0,
            weightII: 0,
          ),
          CardButtonfood()
        ],
      ),
    );
  }
}

class CardProfile extends StatefulWidget {
  CardProfile({Key? key}) : super(key: key);

  @override
  _CardProfileState createState() => _CardProfileState();
}

class _CardProfileState extends State<CardProfile> {
  String imageprofile = '';
  int weight = 55;
  int height = 170;

  @override
  void initState() {
    super.initState();
    imageprofile = FirebaseAuth.instance.currentUser!.photoURL!;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Row(
          children: [
            Container(
              width: 100,
              height: 100,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: NetworkImage(imageprofile), fit: BoxFit.fill)),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Column(
                        children: [
                          Text(
                            "( เหลือแคลอรี่ที่บริโภคได้อีก )",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            " 1700 / 2000 KACL ",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange[400]),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Text('อายุ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text('25',
                                      style: TextStyle(
                                          color: Colors.green[800],
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ))
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Text('เพศ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text('ชาย',
                                      style: TextStyle(
                                          color: Colors.blue[800],
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ))
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Text('ส่วนสูง',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text('170',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ))
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Text('นํ้าหนัก',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    '55',
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ))
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        elevation: 8,
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        shape: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.white, width: 1)),
      ),
    );
  }
}

class CardBMI extends StatefulWidget {
  int weightII;
  int heightII;
  CardBMI({Key? key, required this.weightII, required this.heightII})
      : super(key: key);

  @override
  _CardBMIState createState() => _CardBMIState(this.weightII, this.heightII);
}

class _CardBMIState extends State<CardBMI> {
  TextEditingController weight = new TextEditingController();
  int weightII;
  int heightII;
  String ch_Weight = '57';
  _CardBMIState(this.weightII, this.heightII);
  createaAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('กรุณากรอก นํ้าหนัก ของคุณ'),
            content: TextField(
              controller: weight,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text('บันทึก'),
                onPressed: () {
                  setState(() {
                    ch_Weight = weight.text;
                  });
                  Navigator.of(context).pop(weight.text.toString());
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text("แคลรี่ที่ได้รับจากอาหาร"),
                  ),
                  Row(
                    children: [
                      Image.asset(
                        'images/Food.png',
                        width: 50,
                        height: 50,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Text('300 Kcal'),
                      )
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text(
                              "BMI 19.7",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              "นน.สมส่วน",
                              style: TextStyle(
                                  color: Colors.green[700],
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 50)),
              Column(
                children: [
                  Text(
                    'นํ้าหนักปัจจุบัน $ch_Weight / นํ้าหนักที่ต้องการลด 54 กก.',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            createaAlertDialog(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'บันทึกนํ้าหนัก',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Color(0xff545268)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CalculatorBMI()),
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "คํานวณ BMI",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
      elevation: 8,
      margin: EdgeInsets.all(20),
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: Colors.white, width: 1)),
    );
  }
}

class CardButtonfood extends StatefulWidget {
  CardButtonfood({Key? key}) : super(key: key);

  @override
  _CardButtonfoodState createState() => _CardButtonfoodState();
}

class _CardButtonfoodState extends State<CardButtonfood> {
  Color red = Color(0xFFF86255);
  Color green = Color(0xFF62B56A);
  Color yellow = Color(0xFFFFA041);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              CardshowList('images/Noodel.png', "บะหมี่หมู่", "300 Kcal"),
              CardshowList('images/Pasta.png', "พาสต้ากุ้ง", "150 Kcal"),
              CardshowList('images/I_Cream.png', "ไฮศรีมนมสด", "350 Kcal"),
              CardshowList('images/Noodel.png', "บะหมี่หมู่", "300 Kcal"),
              CardshowList('images/Pasta.png', "พาสต้ากุ้ง", "150 Kcal"),
              CardshowList('images/I_Cream.png', "ไฮศรีมนมสด", "350 Kcal"),
              CardshowList('images/Noodel.png', "บะหมี่หมู่", "300 Kcal"),
              CardshowList('images/Pasta.png', "พาสต้ากุ้ง", "150 Kcal"),
              CardshowList('images/I_Cream.png', "ไฮศรีมนมสด", "350 Kcal"),
              CardshowList('images/Noodel.png', "บะหมี่หมู่", "300 Kcal"),
              CardshowList('images/Pasta.png', "พาสต้ากุ้ง", "150 Kcal"),
              CardshowList('images/I_Cream.png', "ไฮศรีมนมสด", "350 Kcal"),
              CardshowList('images/Noodel.png', "บะหมี่หมู่", "300 Kcal"),
              CardshowList('images/Pasta.png', "พาสต้ากุ้ง", "150 Kcal"),
              CardshowList('images/I_Cream.png', "ไฮศรีมนมสด", "350 Kcal"),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: buttonfood("อาหาร", red, context),
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
            ),
            Container(
              child: buttonfood("ผัก", green, context),
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
            ),
            Container(
              child: buttonfood("ขนม / ผลไม้", yellow, context),
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
            )
          ],
        )
      ],
    );
  }
}

// ignore: non_constant_identifier_names
Widget CardshowList(String imageicon, String namefood, String textkacl) {
  return WidgetCard(
    widgetPadding: 20,
    widget: Column(
      children: [
        Container(
          child: Image.asset(
            imageicon,
            width: 50,
            height: 50,
          ),
        ),
        SizedBox(height: 10),
        Text(namefood),
        SizedBox(height: 5),
        Text(textkacl),
      ],
    ),
  );
}

Widget buttonfood(String name, Color colors, BuildContext context) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(primary: Colors.white),
    onPressed: () {
      if (name == 'อาหาร') {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ListFood()),
        );
      } else if (name == 'ผัก') {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Listvegetable()),
        );
      } else if (name == 'ขนม / ผลไม้') {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ListSnack()),
        );
      }
    },
    child: Container(
      width: 100,
      height: 100,
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            color: Colors.white,
            child: Text(
              "หมวดหมู่",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100,
                height: 50,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0), color: (colors)),
                child: Text(
                  name,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
